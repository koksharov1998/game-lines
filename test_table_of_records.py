import json
import os
import unittest

from table_of_records import sort, get_results


class TestTableOfRecordsMethods(unittest.TestCase):

    def test_right_reading_from_file(self):
        f = open('test.json', 'w')
        json.dump('{\'выпыв\': 1200, \'User\': 600, \'User1\': 1800}', f)
        f.close()
        results = get_results('test.json')
        os.remove('test.json')
        self.assertEqual('{\'выпыв\': 1200, \'User\': 600, \'User1\': 1800}', results)

    def test_sorting(self):
        file = "{\"\u0432\u044b\u043f\u044b\u0432\": 1200, \"User\": 600, \"User1\": 1800}"
        unsorted_results = json.loads(file)
        sorted_results = sort(unsorted_results)
        self.assertEqual('{\'User1\': 1800, \'выпыв\': 1200, \'User\': 600}', str(sorted_results))
