import unittest

import pygame

from brick import Brick
from lines import find_path_bfs


class TestLinesMethods(unittest.TestCase):

    def test_bfs_no_way(self):
        graph = []
        for i in range(2):
            graph.append([])
            for j in range(2):
                if (j + i) % 2 == 1:
                    colour = 1
                else:
                    colour = 0
                rect = pygame.Rect(1, 1, 1, 1)
                graph[i].append(Brick(colour, i, j, rect))

        self.assertEqual(find_path_bfs(graph, graph[0][0], graph[1][1]), [])

    def test_bfs_with_way(self):
        graph = []
        for i in range(2):
            graph.append([])
            for j in range(2):
                if i == 1:
                    colour = 1
                else:
                    colour = 0
                rect = pygame.Rect(1, 1, 1, 1)
                graph[i].append(Brick(colour, i, j, rect))

        self.assertEqual(find_path_bfs(graph, graph[1][0], graph[0][1]), [graph[0][1], graph[0][0], graph[1][0]])

    def test_bfs_start_is_finish(self):
        graph = []
        for i in range(2):
            graph.append([])
            for j in range(2):
                if i == 1:
                    colour = 1
                else:
                    colour = 0
                rect = pygame.Rect(1, 1, 1, 1)
                graph[i].append(Brick(colour, i, j, rect))

        self.assertEqual(find_path_bfs(graph, graph[0][0], graph[0][0]), [graph[0][0]])

    def test_bfs_on_wide_field(self):
        graph = []
        for i in range(5):
            graph.append([])
            for j in range(5):
                if (i == 0 and j == 0) or (i == 1 and j == 1) or (i == 0 and j == 2):
                    colour = 4
                elif i == 2 and j == 1:
                    colour = 3
                else:
                    colour = 0
                rect = pygame.Rect(1, 1, 1, 1)
                graph[i].append(Brick(colour, i, j, rect))

        self.assertEqual(find_path_bfs(graph, graph[2][1], graph[0][1]), [])
        self.assertEqual(find_path_bfs(graph, graph[2][1], graph[1][0]), [graph[1][0], graph[2][0], graph[2][1]])
