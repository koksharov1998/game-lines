import json

import pygame


def main():
    results = get_results('results.json')
    results = sort(results)
    width = 300
    height = 300
    my_font = pygame.font.SysFont('Comic Sans MS', 15)
    window = pygame.display.set_mode((width, height))
    pygame.display.set_caption('Table of Records')
    game_over = False

    while not game_over:
        window.fill((192, 192, 192))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over = True
        i = 1
        for user in results:
            window.blit(my_font.render(str(i), False, (0, 0, 0)), (2, i * 20))
            window.blit(my_font.render(str(results[user]), False, (0, 0, 0)), (120, i * 20))
            window.blit(my_font.render(user, False, (0, 0, 0)), (20, i * 20))
            i += 1
        pygame.display.flip()


def get_results(file_name):
    f = open(file_name, 'r')
    results = json.load(f)
    f.close()
    return results


def sort(dictionary):
    sorted_dict = sorted(dictionary, key=dictionary.get, reverse=True)
    new_dict = {}
    for user in sorted_dict:
        new_dict[user] = dictionary[user]
    return new_dict


if __name__ == '__main__':
    pygame.init()
    main()
    pygame.quit()
