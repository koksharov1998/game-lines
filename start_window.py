import pygame


def main():
    width = 400
    height = 400
    window = pygame.display.set_mode((width, height))
    pygame.display.set_caption('New game')
    font = pygame.font.Font(None, 25)
    count = 0
    mas = [font.render('Enter your name:', False, (255, 255, 255)),
           font.render('Enter field size:', False, (255, 255, 255)),
           font.render('Enter count of colours:', False, (255, 255, 255)),
           font.render('Let\'s go!', False, (255, 255, 255))]
    clock = pygame.time.Clock()
    input_box = pygame.Rect(width / 3 - 50, height / 2 - 50, 200, 30)
    button = pygame.Rect(width / 3, height / 2, 50, 25)
    color_inactive = pygame.Color('lightskyblue3')
    color_active = pygame.Color('dodgerblue2')
    color = color_inactive
    active = False
    input = ''
    settings = []
    alarm = False
    alarm_wrong_colours = False
    alarm_wrong_field_size = False
    alarm_long_name = False
    done = False

    while not done:
        if count == 3:
            return settings
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
                continue
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if input_box.collidepoint(event.pos):
                    active = True
                else:
                    active = False
                if button.collidepoint(event.pos):

                    # Как-то так можно вызвать нажатие enter'а. Только как ???
                    # pygame.event.post(pygame.event.Event(pygame.KEYDOWN))

                    if count == 0 and len(input) > 20:
                        alarm_long_name = True
                    elif count > 0 and not input.isdigit():
                        alarm = True
                        alarm_wrong_field_size = False
                        alarm_wrong_colours = False
                    elif count == 1 and (int(input) <= 3 or int(input) >= 26):
                        alarm_wrong_field_size = True
                        alarm = False
                    elif count == 2 and ((int(input) <= 1) or (int(input) >= 10)):
                        alarm_wrong_colours = True
                        alarm = False
                    else:
                        alarm_long_name = False
                        alarm_wrong_field_size = False
                        alarm_wrong_colours = False
                        alarm = False
                        count += 1
                        settings.append(input)
                        input = ''
                color = color_active if active else color_inactive
            elif event.type == pygame.KEYDOWN:
                if active:
                    if event.key == pygame.K_RETURN:
                        if count == 0 and len(input) > 20:
                            alarm_long_name = True
                        elif count > 0 and not input.isdigit():
                            alarm = True
                            alarm_wrong_field_size = False
                            alarm_wrong_colours = False
                        elif count == 1 and (int(input) <= 3 or int(input) >= 26):
                            alarm_wrong_field_size = True
                            alarm = False
                        elif count == 2 and ((int(input) <= 1) or (int(input) >= 10)):
                            alarm_wrong_colours = True
                            alarm = False
                        else:
                            alarm_long_name = False
                            alarm_wrong_field_size = False
                            alarm_wrong_colours = False
                            alarm = False
                            count += 1
                            settings.append(input)
                            input = ''
                    elif event.key == pygame.K_BACKSPACE:
                        input = input[:-1]
                    else:
                        input += event.unicode

        window.fill((0, 0, 0))
        txt_surface = font.render(input, True, color)
        window.blit(txt_surface, (input_box.x + 5, input_box.y + 5))
        pygame.draw.rect(window, color, input_box, 2)

        pygame.draw.rect(window, (255, 0, 0), button)
        window.blit(font.render('Next', False, (255, 255, 255)), (button.x + 8, button.y + 4))

        window.blit(mas[count], (90, 50))
        if alarm_long_name:
            text_alarm_long_name = font.render('Name is too long name!', False,
                                               (255, 255, 255))
            window.blit(text_alarm_long_name, (width / 3 - 50, height / 2 - 80))
        if alarm_wrong_field_size:
            text_alarm_wrong_field_size = font.render('Size should be more than 3 and less than 26!', False,
                                                      (255, 255, 255))
            window.blit(text_alarm_wrong_field_size, (width / 3 - 115, height / 2 - 80))
        if alarm_wrong_colours:
            text_alarm_wrong_colours = font.render('Colours should be more than 1 and less than 10!', False,
                                                   (255, 255, 255))
            window.blit(text_alarm_wrong_colours, (width / 3 - 130, height / 2 - 80))
        if alarm:
            text_alarm = font.render('It should be a positive integer!', False, (255, 255, 255))
            window.blit(text_alarm, (width / 3 - 80, height / 2 - 80))

        pygame.display.flip()
        clock.tick(30)


if __name__ == '__main__':
    pygame.init()
    main()
    pygame.quit()
