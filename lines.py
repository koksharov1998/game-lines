import json
import random

import pygame

import brick
import start_window
import table_of_records


def find_path_bfs(graph, start_brick, finish_brick):
    length = len(graph)
    if start_brick == finish_brick:
        return [start_brick]
    explored = []
    queue = [[start_brick]]
    while queue:
        path = queue.pop(0)
        node = path[-1]
        if node not in explored:
            neighbours = []
            if node.x + 1 <= length - 1 and (
                    graph[node.x + 1][node.y].colour == (0, 0, 0) or graph[node.x + 1][node.y] == finish_brick):
                neighbours.append(graph[node.x + 1][node.y])
            if node.x - 1 >= 0 and (
                    graph[node.x - 1][node.y].colour == (0, 0, 0) or graph[node.x - 1][node.y] == finish_brick):
                neighbours.append(graph[node.x - 1][node.y])
            if node.y + 1 <= length - 1 and (
                    graph[node.x][node.y + 1].colour == (0, 0, 0) or graph[node.x][node.y + 1] == finish_brick):
                neighbours.append(graph[node.x][node.y + 1])
            if node.y - 1 >= 0 and (
                    graph[node.x][node.y - 1].colour == (0, 0, 0) or graph[node.x][node.y - 1] == finish_brick):
                neighbours.append(graph[node.x][node.y - 1])
            for neighbour in neighbours:
                new_path = list(path)
                new_path.append(neighbour)
                queue.append(new_path)
                if neighbour == finish_brick:
                    new_path.reverse()
                    return new_path
            explored.append(node)
    return []


def main():
    pygame.init()

    settings = start_window.main()
    if settings is None:
        settings = ['User', '8', '5']

    '''
    infoObject = pygame.display.Info()
    pygame.display.set_mode((infoObject.current_w, infoObject.current_h))
    pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
    width = infoObject.current_w
    height = infoObject.current_h
    window = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
    '''
    width = 800
    height = 630
    window = pygame.display.set_mode((width, height))
    height -= 30
    myfont = pygame.font.SysFont('Comic Sans MS', 15)
    user = settings[0]
    text_surface = myfont.render(user, False, (0, 0, 0))
    button_text = myfont.render('HINT', False, (255, 255, 255))
    scores = str(0)
    button = pygame.Rect(width - 50, height + 5, 45, 20)
    pygame.display.set_caption('Lines')
    game_over = False
    clock = pygame.time.Clock()
    window.fill((192, 192, 192))
    colours_count = int(settings[2])
    field_size = int(settings[1])
    cheat_stop_filling_is_on = False
    cheat_go_through_walls_is_on = False
    cheat_destroy_all_is_on = False
    cheat_hint_give_scores_is_on = False

    def random_filling_three(field, colours_count):
        colour = random.randint(1, colours_count)
        length = len(field)
        visited = []
        for i in range(length):
            visited.append([])
            for j in range(length):
                visited[i].append(False)
        j = length ** 2
        i = 0
        extra_scores = 0
        while i < 3:
            x = random.randint(0, length - 1)
            y = random.randint(0, length - 1)
            if field[x][y].colour == (0, 0, 0):
                grey = random.randint(0, 50)
                if grey == 0:
                    field[x][y].change_colour_to(-1)
                    i += 1
                    continue
                field[x][y].change_colour_to(colour)
                extra_scores += check_full_lines(x, y)[1]
                colour = random.randint(1, colours_count)
                i += 1
            elif visited[x][y]:
                j -= 1
            else:
                visited[x][y] = True
            if j == 0:
                return False, 0
        return True, extra_scores

    def check_full_lines(i, j):
        extra_scores = 0
        cells = []
        counter = 0
        try:
            current_colour = field[i][j].colour
            i1 = i
            j1 = j
            while field[i1][j1].colour == current_colour:
                cells.append(field[i1][j1])
                i1 += 1
                counter += 1
        except IndexError:
            pass

        try:
            counter -= 1
            i1 = i
            j1 = j
            while field[i1][j1].colour == current_colour:
                cells.append(field[i1][j1])
                i1 -= 1
                counter += 1
            if counter >= 3:
                for cell in cells:
                    cell.colour = (0, 0, 0)
                extra_scores += counter * 100
                return True, extra_scores
        except IndexError:
            pass

        extra_scores = 0
        cells = []
        counter = 0
        try:
            i2 = i
            j2 = j
            while field[i2][j2].colour == current_colour:
                cells.append(field[i2][j2])
                j2 += 1
                counter += 1
        except IndexError:
            pass
        try:
            counter -= 1
            i2 = i
            j2 = j
            while field[i2][j2].colour == current_colour:
                cells.append(field[i2][j2])
                j2 -= 1
                counter += 1
            if counter >= 3:
                for cell in cells:
                    cell.colour = (0, 0, 0)
                extra_scores += counter * 100
                return True, extra_scores

            return False, 0
        except IndexError:
            return False, 0

    field = []
    for i in range(field_size):
        field.append([])
        for j in range(field_size):
            colour = 0
            rect = pygame.Rect((i * ((width - 5) / field_size) + 5, j * ((height - 5) / field_size) + 5,
                                ((width - 5) / field_size) - 5, ((height - 5) / field_size) - 5))
            field[i].append(brick.Brick(colour, i, j, rect))

    random_filling_three(field, colours_count)

    brick_is_selected = False
    selected_brick_x = -1
    selected_brick_y = -1

    def make_right_turn(field):
        length = len(field)
        for i in range(length):
            for j in range(length):
                current_colour = field[i][j].colour
                if current_colour != (0, 0, 0) and current_colour != (128, 128, 128):
                    field[i][j].colour = (0, 0, 0)
                    for q in range(length):
                        for p in range(length):
                            if field[q][p].colour == (0, 0, 0):
                                path = find_path_bfs(field, field[i][j], field[q][p])
                                if path:
                                    temp = field[q][p].colour
                                    field[q][p].colour = current_colour
                                    check = check_full_lines(q, p)
                                    if check[0]:
                                        field[i][j].colour = temp
                                        return True, check[1]
                                    else:
                                        field[q][p].colour = temp
                    field[i][j].colour = current_colour
        return False, 0

    def draw_field(field):
        length = len(field)
        for i in range(length):
            for j in range(length):
                pygame.draw.rect(window, field[i][j].colour, field[i][j].rect)

    while not game_over:
        clock.tick(10)
        window.fill((192, 192, 192))
        window.blit(text_surface, (10, height))
        window.blit(myfont.render(scores, False, (0, 0, 0)), (75, height))
        pygame.draw.rect(window, (255, 0, 0), button)
        window.blit(button_text, (button.x + 2, button.y - 2))
        draw_field(field)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over = True
                continue
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_t:
                    cheat_stop_filling_is_on = not cheat_stop_filling_is_on
                elif event.key == pygame.K_n:
                    cheat_go_through_walls_is_on = not cheat_go_through_walls_is_on
                elif event.key == pygame.K_i:
                    cheat_hint_give_scores_is_on = not cheat_hint_give_scores_is_on
                elif event.key == pygame.K_BACKSPACE:
                    cheat_destroy_all_is_on = not cheat_destroy_all_is_on
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if button.collidepoint(event.pos):
                    if cheat_hint_give_scores_is_on:
                        scores = str(int(scores) + 100)
                        continue
                    scores = str(int(scores) + make_right_turn(field)[1] - 50)
                    continue
                for i in range(field_size):
                    for j in range(field_size):
                        if field[i][j].rect.collidepoint(event.pos):
                            if cheat_destroy_all_is_on:
                                if field[i][j].colour != (0, 0, 0):
                                    field[i][j].change_colour_to(0)
                                    scores = str(int(scores) + 100)
                                continue
                            elif not brick_is_selected and field[i][j].colour != (0, 0, 0) and field[i][j].colour != (
                                    128, 128, 128):
                                field[i][j].make_darker()
                                brick_is_selected = True
                                selected_brick_x = i
                                selected_brick_y = j
                            elif i == selected_brick_x and j == selected_brick_y:
                                field[i][j].make_brighter()
                                brick_is_selected = False
                                selected_brick_x = -1
                                selected_brick_y = -1
                            elif brick_is_selected and field[i][j].colour != (0, 0, 0) and field[i][j].colour != (
                                    128, 128, 128):
                                field[selected_brick_x][selected_brick_y].make_brighter()
                                selected_brick_x = i
                                selected_brick_y = j
                                field[selected_brick_x][selected_brick_y].make_darker()
                            elif brick_is_selected and field[i][j].colour == (0, 0, 0):
                                if not cheat_go_through_walls_is_on:
                                    path = find_path_bfs(field, field[i][j], field[selected_brick_x][selected_brick_y])
                                else:
                                    path = [field[selected_brick_x][selected_brick_y], field[i][j]]
                                if path:
                                    field[selected_brick_x][selected_brick_y].make_brighter()
                                    temp = field[selected_brick_x][selected_brick_y].colour
                                    for br in range(len(path) - 1):
                                        path[br].colour = (0, 0, 0)
                                        path[br + 1].colour = temp
                                        pygame.draw.rect(window, path[br].colour, path[br].rect)
                                        pygame.draw.rect(window, path[br + 1].colour, path[br + 1].rect)
                                        pygame.time.delay(100)
                                        pygame.display.flip()
                                    pygame.time.delay(200)
                                    brick_is_selected = False
                                    selected_brick_x = -1
                                    selected_brick_y = -1
                                    check = check_full_lines(i, j)
                                    if not check[0] and not cheat_stop_filling_is_on:
                                        check_after_adding = random_filling_three(field, colours_count)
                                        scores = str(int(scores) + check_after_adding[1])
                                        game_over = not check_after_adding[0]
                                        continue
                                    else:
                                        scores = str(int(scores) + check[1])
                                else:
                                    continue
                        else:
                            continue

        pygame.display.flip()

    results = {}

    try:
        f = open('results.json', 'r')
        results = json.load(f)
        f.close()
    except FileNotFoundError:
        pass
    finally:
        f = open('results.json', 'w')
        try:
            if results[user] < int(scores):
                results[user] = int(scores)
        except KeyError:
            results[user] = int(scores)

    json.dump(results, f)
    f.close()

    table_of_records.main()

    pygame.quit()


if __name__ == '__main__':
    pygame.init()
    main()
    pygame.quit()
