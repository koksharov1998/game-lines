class Brick:
    def __init__(self, colour, x, y, rect):
        self.colour = colours[colour]
        self.rect = rect
        self.x = x
        self.y = y

    def make_darker(self):
        self.colour = self.colour[0] * (2 / 4), self.colour[1] * (2 / 4), self.colour[2] * (2 / 4)

    def make_brighter(self):
        self.colour = self.colour[0] * 2, self.colour[1] * 2, self.colour[2] * 2

    def change_colour_to(self, new_colour):
        self.colour = colours[new_colour]


colours = {-1: (128, 128, 128), 0: (0, 0, 0), 1: (255, 0, 0), 2: (0, 255, 0), 3: (0, 0, 255), 4: (255, 255, 0),
           5: (255, 165, 0), 6: (139, 0, 255), 7: (150, 75, 0), 8: (66, 170, 255), 9: (66, 170, 255)}
