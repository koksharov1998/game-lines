import unittest

import brick


class TestBrickMethods(unittest.TestCase):

    def test_make_darker_and_brighter(self):
        b = brick.Brick(1, 0, 0, 0)
        b.make_darker()
        self.assertEqual(b.colour, (127.5, 0, 0))
        b.make_brighter()
        self.assertEqual(b.colour, (255, 0, 0))

    def test_change_colour(self):
        b = brick.Brick(1, 0, 0, 0)
        b.change_colour_to(2)
        self.assertEqual(b.colour, (0, 255, 0))


if __name__ == '__main__':
    unittest.main()
